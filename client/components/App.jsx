import React from 'react';

export default class App extends React.Component {
  constructor() {
    super();
    this.state = { };
  }

  componentDidMount() {
    fetch('/message')
      .then(result=>result.json())
      .then(result => { this.setState(result) });
  }

  render() {
    return (
      <div>
        <h1>{this.state.message}</h1>
      </div>
    )
  }
}