process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('./server');
let should = chai.should();

chai.use(chaiHttp);

describe('GET /message', () => {
  it('it must GET a message', (done) => {
    chai.request(server)
        .get('/message')
        .end((err, res) => {
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.have.property('message');
          done();
        });
    });
});
